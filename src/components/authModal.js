import * as React from "react"
import Modal from "react-modal"
import { StaticImage } from "gatsby-plugin-image"

import "./authModal.css"

const AuthModal = ({ isOpen, onRequestClose }) => {
  return (
    <Modal
      className="auth-modal"
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      closeTimeoutMS={300}
    >
      <div className="auth-modal__wrapper">
        <button className="auth-modal__close" onClick={onRequestClose}>
          <StaticImage
            src="../images/close.png"
            width={120}
            quality={95}
            formats={["auto", "webp", "avif"]}
            alt="Закрыть"
          />
        </button>
        <h3 className="auth-modal__title">Авторизация</h3>

        <p className="auth-modal__desc">для защиты от накруток</p>

        <p className="auth-modal__label">Введите номер телефона*</p>

        <input className="auth-modal__input"></input>
        <br />
        <button className="auth-modal__submit">
          <span className="auth-modal__get-code">Получить код</span>
          <span className="auth-modal__arrow">
            <StaticImage
              src="../images/arrow-small.png"
              width={20}
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Закрыть"
            />
          </span>
        </button>

        <p className="auth-modal__info">
          * — мы не собираем ваши данные и&nbsp;не&nbsp;будем использовать их
          для рассылки&nbsp;рекламы
        </p>
      </div>
    </Modal>
  )
}

Modal.setAppElement("#___gatsby")

export default AuthModal
