import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

import "./sponsors.css"

const Sponsors = () => (
  <div className="sponsors">
    <div>
      <div className="sponsors__wrapper">
        <StaticImage
          src="../images/miet.png"
          width={155}
          quality={95}
          formats={["auto", "webp", "avif"]}
          alt="Спонсор"
        />
      </div>

      <span className="sponsors__text">При поддержке</span>
    </div>
    <div>
      <div className="sponsors__wrapper">
        <StaticImage
          src="../images/nikor.png"
          width={60}
          quality={95}
          formats={["auto", "webp", "avif"]}
          alt="Спонсор"
        />
      </div>
      <span className="sponsors__text">
        Генеральный
        <br />
        спонсор
      </span>
    </div>
    <div>
      <div className="sponsors__wrapper">
        <StaticImage
          src="../images/department.png"
          width={140}
          quality={95}
          formats={["auto", "webp", "avif"]}
          alt="Спонсор"
        />
      </div>
      <span className="sponsors__text">
        Информационный
        <br />
        партнер
      </span>
    </div>
    <div>
      <div className="sponsors__wrapper">
        <StaticImage
          src="../images/nikor.png"
          width={60}
          quality={95}
          formats={["auto", "webp", "avif"]}
          alt="Спонсор"
        />
      </div>
      <span className="sponsors__text">
        Золотой
        <br />
        спонсор
      </span>
    </div>
    <div>
      <div className="sponsors__wrapper">
        <StaticImage
          src="../images/miet.png"
          width={155}
          quality={95}
          formats={["auto", "webp", "avif"]}
          alt="Спонсор"
        />
      </div>

      <span className="sponsors__text">
        Серебрянный
        <br />
        спонсор
      </span>
    </div>
  </div>
)

export default Sponsors
