import * as React from "react"
import { useState } from "react"
import Modal from "react-modal"
import { StaticImage } from "gatsby-plugin-image"

import "./voteModal.css"

const VoteModal = ({ isOpen, onRequestClose }) => {
  const [vote, setVote] = useState(0)

  const candidates = [
    {
      id: 1,
      value: `Никор в 3-м районе`,
      desc: `Современный медицинский центр, осуществляющий диагностику и лечение различных заболеваний с использованием современных методов и передовых медицинских технологий.`,
      link: `nikormed.ru`,
    },
    {
      id: 2,
      value: `Аксис`,
      desc: `Современный медицинский центр, осуществляющий диагностику и лечение различных заболеваний с использованием современных методов и передовых медицинских технологий.`,
      link: `aksis.ru`,
    },
    {
      id: 3,
      value: `Детство плюс`,
      desc: `Современный медицинский центр, осуществляющий диагностику и лечение различных заболеваний с использованием современных методов и передовых медицинских технологий.`,
      link: `detstvopl.ru`,
    },
    {
      id: 4,
      value: `Парацельс`,
      desc: `Современный медицинский центр, осуществляющий диагностику и лечение различных заболеваний с использованием современных методов и передовых медицинских технологий.`,
      link: `paracelss.ru`,
    },
    {
      id: 5,
      value: `Никор в 3-м районе`,
      desc: `Современный медицинский центр, осуществляющий диагностику и лечение различных заболеваний с использованием современных методов и передовых медицинских технологий.`,
      link: `nikormed.ru`,
    },
    {
      id: 6,
      value: `Аксис`,
      desc: `Современный медицинский центр, осуществляющий диагностику и лечение различных заболеваний с использованием современных методов и передовых медицинских технологий.`,
      link: `aksis.ru`,
    },
    {
      id: 7,
      value: `Детство плюс`,
      desc: `Современный медицинский центр, осуществляющий диагностику и лечение различных заболеваний с использованием современных методов и передовых медицинских технологий.`,
      link: `detstvopl.ru`,
    },
    {
      id: 8,
      value: `Парацельс`,
      desc: `Современный медицинский центр, осуществляющий диагностику и лечение различных заболеваний с использованием современных методов и передовых медицинских технологий.`,
      link: `paracelss.ru`,
    },
    {
      id: 9,
      value: `Детство плюс`,
      desc: `Современный медицинский центр, осуществляющий диагностику и лечение различных заболеваний с использованием современных методов и передовых медицинских технологий.`,
      link: `detstvopl.ru`,
    },
    {
      id: 10,
      value: `Парацельс`,
      desc: `Современный медицинский центр, осуществляющий диагностику и лечение различных заболеваний с использованием современных методов и передовых медицинских технологий.`,
      link: `paracelss.ru`,
    },
  ]

  return (
    <Modal
      className="vote-modal"
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      closeTimeoutMS={300}
    >
      <div className="vote-modal__wrapper">
        <button className="vote-modal__close" onClick={onRequestClose}>
          <StaticImage
            src="../images/close.png"
            width={120}
            quality={95}
            formats={["auto", "webp", "avif"]}
            alt="Закрыть"
          />
        </button>

        <div className="vote-modal__header">
          <h3 className="vote-modal__title">Голосование</h3>
          <StaticImage
            src="../images/logo-color.png"
            width={165}
            quality={95}
            formats={["auto", "webp", "avif"]}
            alt="Народная премия"
          />
        </div>

        <div className="vote-modal__body">
          <h4 className="vote-modal__subtitle">Медицинский центр</h4>

          {candidates.map(i => (
            <div
              className={`vote-modal__candidate ${
                vote === i.id ? `vote-modal__candidate--active` : ``
              }`}
              key={i.id}
            >
              <span className="vote-modal__radio-wrapper">
                <input
                  className="vote-modal__radio"
                  name="candidates"
                  type="radio"
                  id={i.id}
                  value={i.id}
                  onChange={() => setVote(i.id)}
                />
                <label className="vote-modal__label" htmlFor={i.id}>
                  {i.value}
                </label>
              </span>
              <p className="vote-modal__radio-desc">{i.desc}</p>
              <a className="vote-modal__link" href={i.link} title={i.value}>
                {i.link}
              </a>
            </div>
          ))}
        </div>

        <div className="vote-modal__footer">
          <button className="vote-modal__back">
            <StaticImage
              src="../images/arrow-back.png"
              width={12}
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Народная премия"
            />

            <span className="vote-modal__back-text">Рестораны</span>
          </button>

          <button className="vote-modal__next">
            <div className="vote-modal__next-text">
              <span className="vote-modal__next-category">
                Спортивные центры
              </span>
              <br />
              <span className="vote-modal__next-left">
                Осталось 3 номинации
              </span>
            </div>
            <StaticImage
              src="../images/arrow.png"
              width={70}
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Далее"
            />
          </button>
        </div>
      </div>
    </Modal>
  )
}

Modal.setAppElement("#___gatsby")

export default VoteModal
