import * as React from "react"
import { useState } from "react"
import { StaticImage } from "gatsby-plugin-image"
import { useBreakpoint } from "gatsby-plugin-breakpoints"

import ProposeButton from "../components/proposeButton"
import ProposeModal from "./ProposeModal"

import "./footer.css"

const Footer = () => {
  const breakpoints = useBreakpoint()

  const [isProposeModalOpen, setIsProposeModalOpen] = useState(false)

  return (
    <footer className="footer">
      <div className="footer__wrapper">
        <div className="footer__grid">
          <div>
            <div className="footer__cell">
              <p className="footer__text">Рекламная служба</p>
              <a className="footer__link" href="mailto:reklama@zelenograd.ru">
                reklama@zelenograd.ru
              </a>
            </div>
            <div className="footer__cell">
              <p className="footer__text">Организаторы</p>
              <p className="footer__text">Иван Иванов</p>
              <a className="footer__link" href="tel:+79998888888">
                +7 (999) 888-88-88
              </a>
              <br />
              <a className="footer__link" href="mailto:award@zelenograd.ru">
                award@zelenograd.ru
              </a>
            </div>
          </div>
          <div>
            <div className="footer__cell">
              <p className="footer__text">© 2021 Зеленоград.ру</p>
            </div>
            <div className="footer__cell">
              <p className="footer__text">
                Свидетельство о регистрации
                <br />
                Эл №ФС 77-30549
              </p>
            </div>
          </div>
          <div style={{ paddingRight: `24px`, textAlign: `right` }}>
            <span
              style={{
                display: `block`,
                marginTop: `-70px`,
                ...(breakpoints.md && {
                  marginTop: `0`,
                }),
              }}
            >
              <StaticImage
                src="../images/logo-small.png"
                width={205}
                quality={95}
                formats={["auto", "webp", "avif"]}
                alt="Награда"
              />
            </span>
          </div>
        </div>
        <ProposeButton onClick={() => setIsProposeModalOpen(true)} />
      </div>

      <ProposeModal
        isOpen={isProposeModalOpen}
        onRequestClose={() => setIsProposeModalOpen(false)}
      />
    </footer>
  )
}

export default Footer
