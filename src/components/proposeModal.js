import * as React from "react"
import { useState } from "react"
import Modal from "react-modal"
import { StaticImage } from "gatsby-plugin-image"

import "./proposeModal.css"

const ProposeModal = ({ isOpen, onRequestClose }) => {
  const [isSuccess, setIsSuccess] = useState(false)

  const onSubmit = e => {
    e.preventDefault()
    setIsSuccess(true)
  }

  return (
    <Modal
      className="propose-modal"
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      closeTimeoutMS={300}
    >
      <div className="propose-modal__wrapper">
        <button className="propose-modal__close" onClick={onRequestClose}>
          <StaticImage
            src="../images/close.png"
            width={120}
            quality={95}
            formats={["auto", "webp", "avif"]}
            alt="Закрыть"
          />
        </button>
        <div className="propose-modal__bg">
          <StaticImage
            src="../images/propose-bg.png"
            width={635}
            quality={95}
            formats={["auto", "webp", "avif"]}
            alt="Фон"
          />
        </div>
        <div className="propose-modal__triangle">
          <StaticImage
            src="../images/propose-triangle.png"
            width={315}
            quality={95}
            formats={["auto", "webp", "avif"]}
            alt="Фон"
          />
        </div>
        <div className="propose-modal__rectangle">
          <StaticImage
            src="../images/propose-rect.png"
            width={350}
            quality={95}
            formats={["auto", "webp", "avif"]}
            alt="Фон"
          />
        </div>
        {!isSuccess ? (
          <form className="propose-modal__content" onSubmit={onSubmit}>
            <h3 className="propose-modal__title">
              Предложить
              <br />
              своего номинанта
            </h3>

            <label className="propose-modal__label" htmlFor="category">
              Категория
            </label>
            <select
              className="propose-modal__select"
              id="category"
              name="category"
            >
              <option>Медицинский центр</option>
              <option>Банк</option>
              <option>Магазин</option>
              <option>Салон красоты</option>
              <option>Центр культуры и развлечений</option>
              <option>Работодатель</option>
              <option>Спортивный центр</option>
              <option>Общепит</option>
              <option> Жилой комплекс</option>
              <option>Образовательный проект</option>
              <option>Автосервис</option>
              <option>Персона</option>
            </select>

            <label className="propose-modal__label" htmlFor="name">
              Название компании
            </label>

            <input
              className="propose-modal__input"
              type="text"
              id="name"
              name="name"
            />

            <label className="propose-modal__label" htmlFor="comment">
              Комментарий
            </label>

            <textarea
              className="propose-modal__textarea"
              name="comment"
              id="comment"
              rows="3"
            />

            <button className="propose-modal__submit">
              <span className="propose-modal__submit-text">Отправить</span>
              <span className="propose-modal__submit-icon">
                <StaticImage
                  src="../images/arrow-small.png"
                  width={17}
                  quality={95}
                  formats={["auto", "webp", "avif"]}
                  alt="Отправить"
                />
              </span>
            </button>
          </form>
        ) : (
          <div className="propose-modal__content">
            <h3 className="propose-modal__title">
              Ваше
              <br />
              предложение
              <br />
              принято!
            </h3>

            <p className="propose-modal__success-text">
              После окончания приема номинантов мы сформируем шорт-лист из
              десяти самых упоминаемых, и приступим к голосованию.
            </p>

            <p className="propose-modal__success-text propose-modal__success-text--mb">
              Вы также можете оставить заявки в других номинациях.
            </p>

            <button className="propose-modal__submit">
              <span className="propose-modal__submit-text">Все понятно</span>
              <span className="propose-modal__submit-number">3</span>
            </button>
          </div>
        )}
      </div>
    </Modal>
  )
}

Modal.setAppElement("#___gatsby")

export default ProposeModal
