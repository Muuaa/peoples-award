import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

import "./news.css"

const News = () => {
  const news = [
    {
      date: `21 ноября`,
      text: `Общий двор 23-го микрорайона: подробный обзор в рейтинге общественных пространств Зеленограда`,
      link: `/`,
    },
    {
      date: `12 ноября`,
      text: `«Мозги» для «Бурана». Как зеленоградские микроэлектронщики участвовали в самой масштабной советской космической программе`,
      link: `/`,
    },
    {
      date: `1 ноября`,
      text: `Жителей семи зеленоградских пятиэтажек переселят в конце следующего года. Сейчас для них строят дома в 9-м и 19-м микрорайонах`,
      link: `/`,
    },
  ]

  return (
    <div className="news">
      {news.map((i, index) => (
        <a className="news__link" href={i.link} key={index}>
          <p className="news__date">{i.date}</p>
          <p className="news__text">{i.text}</p>
        </a>
      ))}

      <div className="news__controls">
        <a href="/" className="news__all">
          Все новости
        </a>

        <button className="news__subscribe">
          <span className="news__subscribe-arrow">
            <StaticImage
              src="../images/arrow.png"
              width={105}
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Подписаться"
            />
          </span>
          <span className="news__subscribe-text">
            Подписаться
            <br />
            на новости
          </span>
        </button>
      </div>
    </div>
  )
}

export default News
