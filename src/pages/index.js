import * as React from "react"
import { useState } from "react"
import { useBreakpoint } from "gatsby-plugin-breakpoints"

import Layout from "../components/layout"
import Seo from "../components/seo"
import Categories from "../components/categories"
import Sponsors from "../components/sponsors"
import Awards from "../components/awards"
import News from "../components/news"
import ProposeButton from "../components/proposeButton"
import AuthModal from "../components/AuthModal"
import VoteModal from "../components/VoteModal"
import ProposeModal from "../components/ProposeModal"

const IndexPage = () => {
  const breakpoints = useBreakpoint()

  const [isAuthModalOpen, setIsAuthModalOpen] = useState(true)
  const [isVoteModalOpen, setIsVoteModalOpen] = useState(true)
  const [isProposeModalOpen, setIsProposeModalOpen] = useState(false)

  return (
    <Layout>
      <Seo title="Главная" />

      <Categories />

      <div
        style={{
          maxWidth: `1140px`,
          margin: `0 auto`,
          padding: breakpoints.md
            ? `64px 16px 32px 16px`
            : `70px 16px 130px 16px`,
          borderBottom: `1px solid #000`,
          textAlign: `center`,
        }}
      >
        <ProposeButton onClick={() => setIsProposeModalOpen(true)} />
      </div>

      <Sponsors />

      <Awards />

      <News />

      <VoteModal
        isOpen={isVoteModalOpen}
        onRequestClose={() => setIsVoteModalOpen(false)}
      />

      <AuthModal
        isOpen={isAuthModalOpen}
        onRequestClose={() => setIsAuthModalOpen(false)}
      />

      <ProposeModal
        isOpen={isProposeModalOpen}
        onRequestClose={() => setIsProposeModalOpen(false)}
      />
    </Layout>
  )
}

export default IndexPage
